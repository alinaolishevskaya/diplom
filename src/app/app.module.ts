import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }   from '@angular/common/http';

import { AppComponent } from './app.component';
import { MainPageComponent } from './main-page/main-page.component';

import {Routes, RouterModule} from '@angular/router';
import {SpeechRecognitionService} from './services/speech-recognition.service';
import { NextComponent } from './next/next.component'

const appRoutes: Routes =[
  { path: 'main', component: MainPageComponent},
  { path: 'next', component: NextComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    NextComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
      FormsModule,
      HttpClientModule
  ],
  providers: [
      SpeechRecognitionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
