import {Component, OnInit, OnDestroy} from '@angular/core';
import {SpeechRecognitionService} from './../services/speech-recognition.service';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/toPromise';


@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit, OnDestroy {

    showSearchButton: boolean;
    speechData: string;
    login: boolean = true;
    showSuccess: false;
    activeLoader: false;
    nameModel: '';
    emailModel: '';
    passModel: string;


    constructor(private speechRecognitionService: SpeechRecognitionService,
                private http: HttpClient) {
        this.showSearchButton = true;
        this.speechData = "";
    }

    ngOnInit() {
        console.log("hello")
    }

    ngOnDestroy() {
        this.speechRecognitionService.DestroySpeechObject();
    }

    activateSpeechSearchMovie(): void {
        this.showSearchButton = false;

        this.speechRecognitionService.record()
            .subscribe(
                //listener
                (value) => {
                    this.speechData = value;
                    console.log(value);
                },
                //errror
                (err) => {
                    console.log(err);
                    if (err.error == "no-speech") {
                        console.log("--restatring service--");
                        this.activateSpeechSearchMovie();
                    }
                },
                //completion
                () => {
                    this.showSearchButton = true;
                    console.log("--complete--");
                    this.activateSpeechSearchMovie();
                });
    }

    public toggleActive() {
        this.login = !this.login;
    };

    public setRegistration() {
        this.passModel = this.speechData.toLowerCase().replace(/\s*/g, '');
        console.log(this.passModel)
        this.http.post('https://enigmatic-ridge-10508.herokuapp.com/api/users', {
            name: this.nameModel,
            email: this.emailModel,
            pass: this.passModel
        }).subscribe(
            result => {
                const res: any = result;
                if (res.code === 200)  {
                    console.log(res)
                    console.log(this.passModel)
                }
            },
            error => {
                alert(error)
            }
        )
    };

    public setLogin () {
        this.passModel = this.speechData.toLowerCase().replace(/\s*/g, '');
        this.http.put('https://enigmatic-ridge-10508.herokuapp.com/api/users', {
            email: this.emailModel,
            pass: this.passModel
        }).subscribe(
            result => {
                const res: any = result;
                if (res.code === 200)  {
                    //location.href = '/next';
                    console.log(res)
                }
                else
                    return false;

            },
            error => {
                alert(error);
                return false;
            }
        )
    }


}
